#!/usr/bin/env python

"""settings.py

Udacity conference server-side Python App Engine app user settings

$Id$

created/forked from conference.py by wesc on 2014 may 24

"""

# Replace the following lines with client IDs obtained from the APIs
# Console or Cloud Console.
#WEB_CLIENT_ID = '919419413765-mesc8sq29rhaokl2ce6dqlpqokpqspe1.apps.googleusercontent.com'
WEB_CLIENT_ID = '919419413765-uacfkbauq1k6hmdovhrb9vcpbo5n63ek.apps.googleusercontent.com'

